let arreglo = [48, 10, 17, 93, 16, 85, 87, 23, 9, 90, 33, 14, 64, 37, 24, 32, 66, 6, 34, 79];
let arregloOrden = [];
let aleatorios = 0;
let par = 0;
let impar = 0;

console.log("El promedio del arreglo es: " + prom(arreglo));
console.log("Hay un total de " + pares(arreglo) + " pares");
console.log("Arreglo ordenado: " + maymen(arreglo));

function prom(arreglo) {
    let promedio = 0;
    for (let i = 0; i < arreglo.length; ++i) {
        promedio = + arreglo[i];
    }
    promedio = promedio / arreglo.length;
    return promedio.toFixed(4);
}

function pares(arreglo) {
    let pares = 0;
    for (let i = 0; i < arreglo.length; ++i) {
        if ((arreglo[i] % 2) == 0) {
            pares++;
        }
    }
    return pares.toFixed(0);
}

function maymen(arreglo) {
    return arreglo.sort(function (may, men) { return men - may });
}

function menmay(arregloOrden){
    return arregloOrden.sort(function(a, b){return a - b});
}

function llenar() {
    limpiar();
    var limite = document.getElementById("limite").value;
    var ListaNumeros = document.getElementById("numeros");
    var porPares = document.getElementById("porPares");
    var porImpares = document.getElementById("porImpares");
    var esSimetrico = document.getElementById("esSimetrico")

    limite == "" ? errorLimite.style.visibility = 'visible' : errorLimite.style.visibility = 'hidden';

    {
        if (limite == "" || document.getElementById('limite').value == "") {
            window.alert(" No introdujo el número de números, o introdujo el caracter no válido 'e'.  Inténtelo de nuevo");
        } else {

        }

        
        random(limite, ListaNumeros, porImpares, porPares, esSimetrico);
    }
}

function random(limite, ListaNumeros, porImpares, porPares, esSimetrico){
    par = 0;
    impar = 0;
    for (let con = 0; con < limite; con++) {
        ListaNumeros.remove(con);
        aleatorios = (Math.random() * 100).toFixed(0);;
        do {
            aleatorios = (Math.random() * 100).toFixed(0);
        } while (aleatorios < 1 || aleatorios > 50);
        arregloOrden[con] = aleatorios;
        console.log(arregloOrden[con]);
        menmay(arregloOrden);
        ListaNumeros.options[con] = new Option(aleatorios, 'valor: ' + aleatorios);
    }

    for (let x = 0; x < limite; x++) {
        console.log(arregloOrden[x]);
        ListaNumeros.options[x] = new Option(arregloOrden[x], 'valor: ' + aleatorios);
    }

    for (let i = 0; i < limite; i++) {
        if ((arregloOrden[i] % 2) != 0) {
            impar = impar + 1;
        } else {
            par = par + 1;
        }
    }

    porImpares = (impar * 100) / limite;
    console.log(porImpares);
    document.getElementById("porImpares").innerHTML = " " + porImpares + "%";
    porPares = (par * 100) / limite;
    console.log(porPares);
    document.getElementById("porPares").innerHTML = " " + porPares + "%";

    if (porImpares > 25 && porImpares < 75) {
        document.getElementById("esSimetrico").innerHTML = "Es simétrico";
    } else {
        document.getElementById("esSimetrico").innerHTML = "No es simétrico";
    }
}

function limpiar(){
    document.getElementById("numeros").innerHTML = "";
}